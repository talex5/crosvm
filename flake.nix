{
  description = "Hacky crosvm fork for qubes-lite";

  outputs = { self, nixpkgs }:
    let pkgs = nixpkgs.legacyPackages.x86_64-linux; in {

    packages.x86_64-linux.crosvm = pkgs.callPackage (import ./default.nix) {};

    packages.x86_64-linux.default = self.packages.x86_64-linux.crosvm;

  };
}
